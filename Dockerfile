FROM alpine:latest

RUN apk --update add \
    openssh && \
    rm -rf /var/cache/apk/*
